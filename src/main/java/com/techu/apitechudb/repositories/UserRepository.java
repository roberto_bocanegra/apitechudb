package com.techu.apitechudb.repositories;

import com.techu.apitechudb.ApitechudbApplication;
import com.techu.apitechudb.models.UserModel;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class UserRepository {

    public List<UserModel> findAll() {
        System.out.println("findAll en UserRepository");
        return ApitechudbApplication.userModels;
    }

    public UserModel save(UserModel user){
        System.out.println("save en UserRepository");
        ApitechudbApplication.userModels.add(user);
        return user;
    }
    public Optional<UserModel> findById(String id){

        System.out.println("findById en UserRepository");

        Optional<UserModel> result = Optional.empty();

        for (UserModel userInList : ApitechudbApplication.userModels){
            if (userInList.getId().equals(id)) {
                System.out.println("usuario encontrado");
                result = Optional.of(userInList);
            }
        }
        return  result;
    }

    public UserModel update(UserModel user){
        System.out.println("update en UserRepository");

        Optional<UserModel> userToUpdate =  this.findById(user.getId());

        if (userToUpdate.isPresent() == true){
            System.out.println("usuario para actualizar encontrado");

            UserModel userFromList = userToUpdate.get();

            userFromList.setName(user.getName());
            userFromList.setAge(user.getAge());
        }
        return user;
    }

    public void delete(UserModel user){
        System.out.println("delete en UserRepository");
        System.out.println("Borrando usuario");

        ApitechudbApplication.userModels.remove(user);
    }

}
