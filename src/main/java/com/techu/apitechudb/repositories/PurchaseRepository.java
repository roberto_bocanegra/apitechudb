package com.techu.apitechudb.repositories;

import com.techu.apitechudb.ApitechudbApplication;
import com.techu.apitechudb.models.PurchaseModel;
import com.techu.apitechudb.models.UserModel;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class PurchaseRepository {

    public PurchaseModel save(PurchaseModel purchase){
        System.out.println("save en UserRepository");
        ApitechudbApplication.purchaseModels.add(purchase);
        return purchase;
    }

    public List<PurchaseModel> findAll() {
        System.out.println("findAll em PurchaseRepository");

        return ApitechudbApplication.purchaseModels;

    }

    public Optional<PurchaseModel> findById(String id){
        System.out.println("findById en purchaseRepo");
        System.out.println("la id es " + id);

        Optional<PurchaseModel> result = Optional.empty();

        for (PurchaseModel purchaseInList : ApitechudbApplication.purchaseModels){
            if (purchaseInList.getId().equals(id)) {
                System.out.println("compra encontrado");
                result = Optional.of(purchaseInList);
            }
        }
        return  result;


    }
}
