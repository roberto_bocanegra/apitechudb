package com.techu.apitechudb.controllers;


import com.techu.apitechudb.models.PurchaseModel;
import com.techu.apitechudb.services.PurchaseService;
import com.techu.apitechudb.services.PurchaseServiceResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/apitechu/v2")
public class PurchaseController {
    @Autowired
    PurchaseService purchaseService;

    @GetMapping("/purchase")
    public ResponseEntity<List<PurchaseModel>> getPurchase(){
        System.out.println("getPurchases");
        return new ResponseEntity<>(
                this.purchaseService.getPurchase(),
                HttpStatus.OK
        );
    }

    @PostMapping("/purchase")
    public ResponseEntity<PurchaseModel> addPurchase(@RequestBody PurchaseModel purchase) {
        System.out.println("addPurchase");
        System.out.println("la id      del la compra es : " + purchase.getId());
        System.out.println("la id  del usuario es : " + purchase.getUserId());
        System.out.println("los elementos del la compra son : " + purchase.getPurchaseItems());

        PurchaseServiceResponse purchaseServiceResponse = this.purchaseService.addPurchase(purchase);

        return new ResponseEntity<>(
                purchaseServiceResponse.getPurchase(),
                purchaseServiceResponse.getResponseHttpStatusCode()
        );
    }

}
