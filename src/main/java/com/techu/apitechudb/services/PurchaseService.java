package com.techu.apitechudb.services;

import com.techu.apitechudb.models.PurchaseModel;
import com.techu.apitechudb.repositories.PurchaseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class PurchaseService {
    @Autowired
    PurchaseRepository purchaseRepository;

    @Autowired
    UserService userService;

    @Autowired
    ProductService productService;

    public PurchaseServiceResponse addPurchase(PurchaseModel purchase){

        System.out.println("add en PurchaseService");

        PurchaseServiceResponse result = new PurchaseServiceResponse();
        result.setPurchase(purchase);

        if (this.userService.findById(purchase.getUserId()).isPresent()== false){

            System.out.println("el usuario de la compra no se ha encontrado");
            result.setMsg("el usuario de la compra no se ha encontrado");
            result.setResponseHttpStatusCode(HttpStatus.BAD_REQUEST);

            return result;
        }
        if (this.getById(purchase.getId()).isPresent() == true ){
            System.out.println("hay una compra con esta ID");
            result.setMsg("hay una compra con esta ID");
            result.setResponseHttpStatusCode(HttpStatus.BAD_REQUEST);
            return result;
        }

        float amount = 0;
        for(Map.Entry<String, Integer> purchaseItem : purchase.getPurchaseItems().entrySet()){

            if (!this.productService.findById(purchaseItem.getKey()).isPresent()){
                System.out.println("el producto con la id" + purchaseItem.getKey() + "no se ha encontrado");
                result.setMsg("el producto con la id" + purchaseItem.getKey() + "no se ha encontrado");
                result.setResponseHttpStatusCode(HttpStatus.BAD_REQUEST);

            } else {
                System.out.println("añadiendo valor de " + purchaseItem.getValue() + "unidades del producto al total");

                amount +=
                        (this.productService.findById(purchaseItem.getKey())).get().getPrice() * purchaseItem.getValue();
            }
        }
        purchase.setAmount(amount);
        this.purchaseRepository.save(purchase);
        result.setMsg("compra añadida correctamente");
        result.setResponseHttpStatusCode(HttpStatus.OK);

        return result;
    }

    public List<PurchaseModel> getPurchase() {
        System.out.println("getPurchase en PurchaseService");

        return this.purchaseRepository.findAll();

    }

    public Optional<PurchaseModel> getById(String id){
        System.out.println("getById en PurchaseService");
        System.out.println("La id es : " + id);

        return this.purchaseRepository.findById(id);
    }
}
