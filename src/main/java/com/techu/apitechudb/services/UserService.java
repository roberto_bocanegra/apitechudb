package com.techu.apitechudb.services;

import com.techu.apitechudb.models.UserModel;
import com.techu.apitechudb.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    public List<UserModel> findAll() {
        System.out.println("findAll en UserService");

        return this.userRepository.findAll();
    }

    public UserModel add(UserModel user){

        System.out.println("add en UserService");

        return this.userRepository.save(user);
    }

    public Optional<UserModel> findById(String id){

        System.out.println("findById en UserService");

        return this.userRepository.findById(id);
    }
    public UserModel update(UserModel userModel){
        System.out.println("update en userService");

        return this.userRepository.update(userModel);
    }


    public boolean delete(String id) {
        System.out.println("delete en Userservice");

        boolean result = false;

        Optional<UserModel> userToDelete = this.findById(id);

        if (userToDelete.isPresent() == true) {

            result = true;
            this.userRepository.delete(userToDelete.get());
        }
        return result;

    }

}
